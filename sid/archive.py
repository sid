# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2020 kaliko <kaliko@azylum.org>
# SPDX-License-Identifier: GPL-3.0-or-later
"""Fetch packages info from the archive

>>> from sid.archive import Archive
"""

from re import compile as re_compile

from .plugin import Plugin, botcmd
from .lib import get_pkg


class Archive(Plugin):
    """Fetches package info from the archive
    """
    #: Current stable Suite
    stable_codename = 'buster'
    #: Pakage name regexp
    re_pkg = re_compile(r'(?P<package>[0-9a-z.+-]+)$')

    def __init__(self, bot):
        Plugin.__init__(self, bot)

    @botcmd
    def archive(self, rcv, args):
        """Fetches package info from the archive

        ``!archive pkg-name`` : Returns package versions (by suite)"""
        if not args:
            return
        if len(args) > 1:
            self.log.info('more than one packages provided')
        pkg = Archive.re_pkg.match(args[0])
        if not pkg:
            msg = 'Wrong package name format re: "{}"'.format(
                Archive.re_pkg.pattern)
            self.reply(rcv, msg)
            return
        pkg_name = pkg.groupdict('package').get('package')
        info = get_pkg(pkg_name)
        if not info:
            self.reply(rcv, 'pakage not found')
            return
        messages = []
        suites_av = set(info.keys() & {'stable', 'testing', 'unstable',
                                       f'{Archive.stable_codename}-backports'})
        for suite in sorted(suites_av):
            cpnt = '/'.join({v['component'] for v in info[suite].values()})
            versions = '/'.join(info[suite].keys())
            messages.append(f'{suite:16}: {versions} {cpnt}')
        msg = '\n'.join(messages)
        self.reply(rcv, msg)
