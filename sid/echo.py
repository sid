# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2007-2012 Thomas Perl <thp.io/about>
# SPDX-FileCopyrightText: 2014, 2020 kaliko <kaliko@azylum.org>
# SPDX-License-Identifier: GPL-3.0-or-later

from .plugin import Plugin, botcmd


class Echo(Plugin):
    """Drops a message to be sent when someone gets online.
    """

    def __init__(self, bot):
        Plugin.__init__(self, bot)
        self.inbox = dict()
        self.presence = dict()
        # The groupchat_presence event is triggered whenever a
        # presence stanza is received from any chat room, including
        # any presences you send yourself. To limit event handling
        # to a single room, use the events muc::room@server::presence,
        # muc::room@server::got_online, or muc::room@server::got_offline.
        bot.add_event_handler("muc::%s::presence" %
                              self.bot.room, self.log_presence)

    def log_presence(self, pres):
        """Handler method registering MUC participants presence"""
        self.log.debug('%s: %s', pres['muc']['nick'], pres['type'])
        nick = pres['muc']['nick']
        self.presence.update({nick: (pres['muc']['role'], pres['type'])})
        self.log.debug(self.presence)
        if pres['type'] == 'available':
            while self.inbox.get(nick, []):
                self.send(self.bot.room,
                          self.inbox.get(nick).pop(),
                          mtype='groupchat')
                self.inbox.pop(nick)

    @botcmd
    def tell(self, message, args):
        """Drops a message to be sent when someone gets online.

        * ``!tell queue``       : messages in queue
        * ``!tell <nick> <msg>``: append <msg> to <nick> in queue"""
        if not len(args):
            msg = 'Missing arguments:\n{}'.format(self.tell.__doc__)
            self.reply(message, msg)
            return
        if len(args) == 1:
            if args[0] == 'queue':
                msg = '\n'.join(['{0}:\n\t{1}'.format(k, '\n'.join(v))
                                 for k, v in self.inbox.items()])
                self.reply(message, msg)
                return
            if args[0] == 'purge':
                sender = message['from'].resource
                if self.presence[sender][0] == 'moderator':
                    self.inbox = dict()
                return
        if len(args) < 2:
            msg = 'Please provide a message:\n{}'.format(self.tell.__doc__)
            self.reply(message, msg)
            return
        self._handle_msg(message)

    def _handle_msg(self, message):
        """Format and drop message in the inbox"""
        sender = message['from'].resource
        recipient = message['body'].split()[1]
        tell_msg = ' '.join(message['body'].split()[2:])
        self.log.debug('%s: %s', recipient, tell_msg)
        letter = '{0}, {1} wanted you to know: {2}'.format(recipient, sender, tell_msg)
        if (self.presence.get(recipient) and
                self.presence[recipient][1] == 'available'):
            return
        if recipient in self.inbox.keys():
            self.inbox[recipient].append(letter)
        else:
            self.inbox[recipient] = [letter]


# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab
