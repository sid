# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2010, 2011 Anaël Verrier <elghinn@free.fr>
# SPDX-FileCopyrightText: 2014, 2020, 2023 kaliko <kaliko@azylum.org>

import logging

from slixmpp.exceptions import XMPPError

from .sid import botcmd


class Plugin:
    """
    Simple Plugin object to derive from:

       * Exposes the bot object and its logger
       * Provides send helpers

    :param sid.sid.MUCBot bot: bot the plugin is load from
    """
    #: Overriding bot log level for the plugin
    log_level = None

    def __init__(self, bot):
        self.bot = bot
        self.log = bot.log.getChild(self.__class__.__name__)
        #: :py:obj:`list` : List of tuples (event, handler)
        self.handlers = []
        if self.log_level:
            self.log.setLevel(self.log_level)

    def add_handlers(self):
        """Add handlers declared in self.hanlders"""
        for event, handler in self.handlers:
            self.log.debug(f'Add {event} > {self.__class__.__name__}().{handler.__name__}')
            self.bot.add_event_handler(event, handler)

    def rm_handlers(self):
        """Remove handlers declared in self.hanlders"""
        for event, handler in self.handlers:
            self.log.debug(f'Remove {event} > {self.__class__.__name__}().{handler.__name__}')
            self.bot.del_event_handler(event, handler)

    def send(self, dest, msg, mtype='chat'):
        """Send msg to dest

        :param str dest: Message recipient
        :param dict,str msg: Message to send (use dict for xhtml-im)

        .. note::
          if **msg** is a :py:obj:`dict` to provide xhmlt-im massages::

              msg = {
                  mbody: 'text',
                  mhtml: '<b>text</b>,  # optional'
              }
        """
        if isinstance(msg, str):
            msg = {'mbody': msg}
        msg.setdefault('mhtml', None)
        self.bot.send_message(mto=dest,
                              mtype=mtype,
                              **msg)

    def reply(self, rcv, msg):
        """Smart reply to message received.

        Replies ``msg`` in private or on the muc depending on ``rcv``

        :param rcv: The received message (slixmpp object)
        :param dict,str msg: The message to reply, refer to :py:obj:`sid.plugin.Plugin.send` for ``msg`` format
        """
        to = rcv['from']
        if rcv['type'] == 'groupchat':
            to = rcv['mucroom']
        self.send(to, msg, mtype=rcv['type'])

    async def ban(self, jid, reason):
        """Coroutine to ban a jid from the room

        :param str jid: JID to ban
        :param str reason: Reason
        """
        room = self.bot.room
        try:
            await self.bot['xep_0045'].set_affiliation(room,
                    jid=jid, affiliation='outcast', reason=reason)
        except XMPPError as error:
            self.log.error(error)

    def shutdown(self):
        """Empty method to override. Called on bot shutdown"""
        pass
