# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2020 kaliko <kaliko@azylum.org>
# SPDX-License-Identifier: GPL-3.0-or-later

from datetime import datetime
from os import fdopen
from tempfile import mkstemp
from time import time

from .plugin import Plugin, botcmd


class Log(Plugin):
    """Logs group chat participant presence.

    The account running the bot need at least room moderation right to log
    participants JIDs (fallback to nickname).
    """
    throttle_ts = int(time())-30

    def __init__(self, bot):
        Plugin.__init__(self, bot)
        self.presence = list()
        bot.add_event_handler("muc::%s::presence" %
                              self.bot.room, self.log_presence)
        bot.add_event_handler("muc::%s::got_online" %
                              self.bot.room, self.log_online)
        bot.add_event_handler("muc::%s::got_offline" %
                              self.bot.room, self.log_offline)

    def _get_jid(self, pres):
        nick = pres['muc']['nick']
        jid = self.bot.plugin['xep_0045'].rooms[self.bot.room][nick]['jid']
        if not jid:
            self.log.debug('jid not found, is bot account moderating room?')
            return nick
        #jid = jid.split('/')[0]  # strip ressource
        return jid

    def log_online(self, pres):
        jid = self._get_jid(pres)
        self.log.info('got online: %s as %s', jid, pres['muc']['nick'])
        self.presence.append((datetime.now(), 'online', jid))

    def log_offline(self, pres):
        jid = self._get_jid(pres)
        self.log.info('got offline: %s was %s', jid, pres['muc']['nick'])
        self.presence.append((datetime.now(), 'offline', jid))

    def log_presence(self, pres):
        nick = self._get_jid(pres)
        self.log.debug('%s: %s', pres['muc']['nick'], pres['type'])
        self.presence.append((datetime.now(), pres['type'], nick))

    def _format_log(self):
        log = [f'{d:%Y-%m-%d %H:%M}: {s:12} {n}' for d, s, n in self.presence]
        return '\n'.join(log)

    @botcmd(hidden=True)
    def write(self, message, args):
        """Dump/save room presences log

        ``!write`` : Writes log to file (use mktemp and return file location as MUC message)"""
        delay = int(time()) - Log.throttle_ts
        if delay < 30:
            self.log.debug('throttling file creation')
            self.reply(message, f'wait {30-delay}s')
            return
        log = self._format_log()
        mode = {'mode': 'w', 'encoding': 'utf-8'}
        prefix = self.bot.room.split('@')[0] + '_'
        tmp = mkstemp(prefix=prefix, text=True)
        self.log.info('Writing to %s', tmp[1])
        with fdopen(tmp[0], **mode) as fileo:
            fileo.writelines(log)
            Log.throttle_ts = int(time())
        self.reply(message, tmp[1])

    @botcmd(hidden=True)
    def dump(self, message, args):
        """``!dump``  : Dumps log as MUC message"""
        self.reply(message, self._format_log())


# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab
