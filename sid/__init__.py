# -*- coding: utf-8 -*-
"""Module info
"""

__author__ = 'kaliko'
__email__ = 'kaliko@azylum.org'
__description__ = 'An xmpp bot based on slixmpp'
# Use semantic versioning as much as possible
# https://www.python.org/dev/peps/pep-0440/
__version__ = '0.4.0'
__url__ = 'http://git.kaliko.me/?p=sid.git'
__doc__ = 'https://kaliko.gitlab.io/sid/'
