#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A MUC ban bot based on "Real-Time Block List" service.
"""

import getpass
import logging

from argparse import ArgumentParser
from getpass import getpass

from ..sid import MUCBot
from ..rtbl import RTBL


def main():
    # Setup the command line arguments.
    parser = ArgumentParser()

    # Output verbosity options.
    parser.add_argument("-q", "--quiet", help="set logging to ERROR",
                        action="store_const", dest="loglevel",
                        const=logging.ERROR, default=logging.INFO)
    parser.add_argument("-d", "--debug", help="set logging to DEBUG",
                        action="store_const", dest="loglevel",
                        const=logging.DEBUG, default=logging.INFO)

    # JID and password options.
    parser.add_argument("-j", "--jid", dest="jid",
                        help="JID to use")
    parser.add_argument("-p", "--password", dest="password",
                        help="password to use")
    parser.add_argument("-r", "--room", dest="room",
                        help="Room to join")
    parser.add_argument("-n", "--nick", dest="nick",
                        default="sid-rtbl",
                        help="Bot nickname")
    parser.add_argument("-b", "--rtbl", dest="rtbl",
                        help="Real-Time Block List (RTBL) service to use (ie. xmppbl.org)")

    args = parser.parse_args()

    # Setup logging.
    logging.basicConfig(level=args.loglevel,
                        format='%(levelname)-8s %(message)s')

    if args.jid is None:
        args.jid = input("Username: ")
    if args.password is None:
        args.password = getpass("Password: ")
    if args.room is None:
        args.room = input("Room: ")
    if args.rtbl is None:
        args.rtbl = input("Real-Time Block List to use: ")

    RTBL.pubsub_server = args.rtbl
    # Instanciate the bot
    xmpp = MUCBot(args.jid, args.password, args.room, args.nick)
    # Register plugin
    xmpp.register_bot_plugin(RTBL)

    # Connect to the XMPP server and start processing XMPP stanzas.
    try:
        xmpp.connect()
        xmpp.process()
    except KeyboardInterrupt:
        xmpp.shutdown_plugins()


if __name__ == '__main__':
    main()
