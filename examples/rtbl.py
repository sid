#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A MUC ban bot based on "Real-Time Block List" service.

Setup a virtualenv:

.. code:: bash
    git clone -b dev https://gitlab.com/kaliko/sid.git && cd sid
    python3 -m venv venv
    . ./venv/bin/activate
    pip install -U pip wheel
    pip install slixmpp==1.8.3
    cp examples/rtbl.py ./my-rtbl-bot.py


Run:

.. code:: bash
    ./venv/bin/python3 my-rtbl-bot.py
"""

import getpass
import logging

from argparse import ArgumentParser
from getpass import getpass

from sid.sid import MUCBot
from sid.ping import Ping
from sid.rtbl import RTBL


if __name__ == '__main__':
    # Setup the command line arguments.
    parser = ArgumentParser()

    # Output verbosity options.
    parser.add_argument("-q", "--quiet", help="set logging to ERROR",
                        action="store_const", dest="loglevel",
                        const=logging.ERROR, default=logging.INFO)
    parser.add_argument("-d", "--debug", help="set logging to DEBUG",
                        action="store_const", dest="loglevel",
                        const=logging.DEBUG, default=logging.INFO)

    # JID and password options.
    parser.add_argument("-j", "--jid", dest="jid",
                        help="JID to use")
    parser.add_argument("-p", "--password", dest="password",
                        help="password to use")
    parser.add_argument("-r", "--room", dest="room",
                        help="Room to join")
    parser.add_argument("-n", "--nick", dest="nick",
                        help="Bot nickname")
    parser.add_argument("-b", "--rtbl", dest="rtbl",
                        help="Real-Time Block List (RTBL) service to use (ie. xmppbl.org)")

    args = parser.parse_args()

    # Setup logging.
    logging.basicConfig(level=args.loglevel,
                        format='%(levelname)-8s %(message)s')

    if args.jid is None:
        args.jid = input("Username: ")
    if args.password is None:
        args.password = getpass("Password: ")
    if args.room is None:
        args.room = input("Room: ")
    if args.nick is None:
        args.nick = input("Bot nickname: ")
    if args.rtbl is None:
        args.rtbl = input("Real-Time Block List to use: ")

    RTBL.pubsub_server = args.rtbl
    # Instanciate the bot
    xmpp = MUCBot(args.jid, args.password, args.room, args.nick)
    # Register plugins
    xmpp.register_bot_plugin(Ping)
    xmpp.register_bot_plugin(RTBL)

    # Connect to the XMPP server and start processing XMPP stanzas.
    try:
        xmpp.connect()
        xmpp.process()
    except KeyboardInterrupt:
        xmpp.shutdown_plugins()
