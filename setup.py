#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup

from sid import __url__, __author__, __email__, __description__

setup(
    name='sid',
    author=__author__,
    author_email=__email__,
    download_url=__url__,
    description=__description__,
)

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab
