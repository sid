Development documentation
=========================

The Bot
-------

.. autoclass:: sid.sid.MUCBot
    :members:
    :undoc-members:
    :show-inheritance:

.. need sphinx v2.0 for autodecorator
.. autodecorator:: sid.sid.botcmd(hidden, name)

Plugin base
-----------

The plugin class to derive from.

.. autoclass:: sid.plugin.Plugin
    :members:
    :undoc-members:

.. vim: spell spelllang=en


Available plugins
-----------------

Generic plugins
~~~~~~~~~~~~~~~

.. automodule:: sid.echo
    :members:
    :show-inheritance:

.. autoclass:: sid.feeds.Feeds
    :members:
    :show-inheritance:

.. automodule:: sid.log
    :members:
    :show-inheritance:

.. autoclass:: sid.rtbl.RTBL
    :members:
    :show-inheritance:

Debian plugins
~~~~~~~~~~~~~~

.. automodule:: sid.archive
    :members:
    :show-inheritance:

.. automodule:: sid.bts
    :members:
    :show-inheritance:

.. vim: spell spelllang=en
