.. sid documentation master file, created by
   sphinx-quickstart on Tue May  5 12:46:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
.. _home:

.. include:: ../../README.rst


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   self
   rtbl.rst
   plugins.rst
   doc.rst
   contribute.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
