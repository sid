===============
sid an xmpp bot
===============

Getting sid
------------

Currently unreleased on PyPi_.

`git tagged` version and `master branch` should be stable though.

* Doc:   https://kaliko.gitlab.io/sid/
* git:   https://gitlab.com/kaliko/sid.git
* Issue: https://gitlab.com/kaliko/sid/-/issues


Install/update master from git::

  pip install -U git+https://gitlab.com/kaliko/sid.git@master#egg=sid

Using the bot
-------------

Here is a plain example::

   #!/usr/bin/env python3
   # -*- coding: utf-8 -*-

   import getpass
   import logging
   from sid.sid import MUCBot
   from sid.ping import Ping

   logging.basicConfig(level=logging.INFO,
                       format='%(levelname)-8s %(message)s')

   JID = 'bot@example.org'
   ROOM = 'room@conf.example.org'
   # Pay attention: if you join the room with the same account/nick,
   # For instance with a regular interactive client, the bot will
   # not answer you, it ignores its own messages for obvious reasons.
   NICK = 'sid'
   PASS = getpass.getpass(f'Password for "{JID}": ')

   # Instanciate the bot
   xmpp = MUCBot(JID, PASS, ROOM, NICK)
   # Register a plugin
   xmpp.register_bot_plugin(Ping)

   # Connect to the XMPP server and start processing XMPP stanzas.
   try:
       xmpp.connect()
       xmpp.process()
   except KeyboardInterrupt:
       xmpp.shutdown_plugins()


Contacting author
-----------------

The current maintainer can be found on xmpp chat room <kaliko.me⊘conf.azylum.org>
or you can contact him by email/xmpp <kaliko⊘azylum.org>.

.. _PyPI: https://pypi.org
